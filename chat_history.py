import openai
import json
import os
from config import OPENAI_API_KEY

openai.api_key = OPENAI_API_KEY

def load_chat_history(filename):
    if os.path.exists(filename):
        with open(filename, "r") as file:
            return json.load(file)
    else:
        return []

def save_chat_history(filename, chat_history):
    with open(filename, "w") as file:
        json.dump(chat_history, file, indent=4)

def chat_with_gpt(prompt, chat_history=[]):
    messages = [
        {"role": "system", "content": "You are ChatGPT, a large language model trained by OpenAI."},
        *chat_history,
        {"role": "user", "content": prompt}
    ]
    
    # An die API senden
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",  # oder "gpt-4" je nach Verfügbarkeit und Zugang
        messages=messages
    )
    
    # Antwort extrahieren
    response_message = response['choices'][0]['message']
    chat_history.append({"role": "user", "content": prompt})
    chat_history.append({"role": "assistant", "content": response_message['content']})
    
    return response_message['content'], chat_history

# Lade den bestehenden Chat-Verlauf oder initialisiere einen neuen
chat_history_file = "chat_history.json"
chat_history = load_chat_history(chat_history_file)

# Neue Interaktion
user_input = "Kannst du mir etwas über Künstliche Intelligenz erzählen?"
response, chat_history = chat_with_gpt(user_input, chat_history)
print("ChatGPT:", response)

# Weitere Interaktion
user_input = "Was sind die aktuellen Trends in der KI?"
response, chat_history = chat_with_gpt(user_input, chat_history)
print("ChatGPT:", response)

# Speichern des aktualisierten Chat-Verlaufs
save_chat_history(chat_history_file, chat_history)

# Speichern des gesamten Dialogverlaufs im Markdown-Format
with open("chat_history.md", "w") as file:
    file.write("# Chat-Verlauf\n\n")
    for message in chat_history:
        role = "Nutzer" if message['role'] == "user" else "ChatGPT"
        file.write(f"**{role}:** {message['content']}\n\n")

