import openai
from config import OPENAI_API_KEY

openai.api_key = OPENAI_API_KEY

def chat_with_gpt(prompt, chat_history=[]):
    messages = [
        {"role": "system", "content": "You are ChatGPT, a large language model trained by OpenAI."},
        *chat_history,
        {"role": "user", "content": prompt}
    ]
    
    # An die API senden
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",  # oder "gpt-4" je nach Verfügbarkeit und Zugang
        messages=messages
    )
    
    # Antwort extrahieren
    response_message = response['choices'][0]['message']
    chat_history.append({"role": "user", "content": prompt})
    chat_history.append({"role": "assistant", "content": response_message['content']})
    
    return response_message['content'], chat_history

# Initialisiere den Dialogverlauf
chat_history = []

# Beispielinteraktion
user_input = "Hallo, wie geht es dir?"
response, chat_history = chat_with_gpt(user_input, chat_history)
print("ChatGPT:", response)

# Weitere Interaktion
user_input = "Kannst du mir etwas über Künstliche Intelligenz erzählen?"
response, chat_history = chat_with_gpt(user_input, chat_history)
print("ChatGPT:", response)

# Speichern des gesamten Dialogverlaufs im Markdown-Format
with open("chat_history.md", "w") as file:
    file.write("# Chat-Verlauf\n\n")
    for message in chat_history:
        role = "Nutzer" if message['role'] == "user" else "ChatGPT"
        file.write(f"**{role}:** {message['content']}\n\n")

