# Mein ChatGPT Projekt

Dies ist ein einfaches Python-Projekt, das die OpenAI GPT-4 API verwendet, um einen Dialog zu führen und diesen Dialog im Markdown-Format zu speichern.


## Voraussetzungen

- Python 3.6 oder höher
- Ein OpenAI API-Schlüssel
- Ein GitLab-Konto (optional für Versionskontrolle)

## Installation

1. **Repository klonen:**
   ```bash
   git clone GITLAB_URL_DEINES_REPOSITORYS
   cd mein_chatgpt_projekt

